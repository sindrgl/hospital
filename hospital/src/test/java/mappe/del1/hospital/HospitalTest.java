package mappe.del1.hospital;

import mappe.del1.hospital.healthpersonal.Nurse;
import mappe.del1.hospital.healthpersonal.doctor.Surgeon;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 *Testing class for remove method in Department class.
 * @author Sindre Glomnes
 * @version 2021.03.04
 */
public class HospitalTest {
    Department emergency = new Department("Akutten");
    Department childrenPolyclinic = new Department("Barn poliklinikk");

    @Nested
    class RemoveTest {

        @BeforeEach
        void addPeople() {
            emergency.addPatient(new Patient("Lisa", "Bambu","5678"));
            childrenPolyclinic.addEmployee(new Surgeon("Jørgen", "Myr", "1122"));
            childrenPolyclinic.addEmployee(new Nurse("Andreas", "vikk", "2244"));

        }

        @Test
        @DisplayName("Can remove a existing employee")
        void removeEmployee(){
            Employee sindre = new Employee("Sindre", "Glomnes", "1234");
            Department emergency = new Department("Akutten");
            emergency.addEmployee(sindre);
            assertDoesNotThrow(() -> emergency.remove(sindre));
        }

        @Test
        @DisplayName("Can remove a existing patient")
        void removePatient(){
            Patient sindre = new Patient("Sindre", "Glomnes", "1234");
            Department childrenPolyclinic = new Department("Barn poliklinikk");
            childrenPolyclinic.addPatient(sindre);
            assertDoesNotThrow(() -> childrenPolyclinic.remove(sindre));
        }

        @Test
        @DisplayName("Can't remove a non existing employee, even when just one letter is different")
        void removeNonExistingEmployee(){
            Employee sindre = new Employee("Sindre", "Glomnes", "1234");
            Employee sondre = new Employee("Sondre", "Glomnes", "1234");
            Department emergency = new Department("Akutten");
            emergency.addEmployee(sindre);
            assertThrows(RemoveException.class, () -> emergency.remove(sondre));
        }

        @Test
        @DisplayName("Can't remove a non existing patient")
        void removeNonExistingPatient(){
            Patient sindre = new Patient("Sindre", "Glomnes", "1234");
            Patient sondre = new Patient("Sondre", "Glomnes", "1234");
            Department childrenPolyclinic = new Department("Akutten");
            childrenPolyclinic.addPatient(sindre);
            assertThrows(RemoveException.class, () -> childrenPolyclinic.remove(sondre));
        }
    }

    @Nested
    class CreateObjectTest {

        @Test
        @DisplayName("Creating a hospital with a name")
        void createHopital() {
            assertDoesNotThrow(() -> new Hospital("Test hospital"));
        }

        @Test
        @DisplayName("Creating a hospital without a name throws IllegalArgumentException")
        void createHospitalWithoutName() {
            assertThrows(IllegalArgumentException.class, () -> new Hospital(""));
        }

        @Test
        @DisplayName("Creating a department with a name")
        void createDepartment() {
            assertDoesNotThrow(() -> new Department("Test department"));
        }

        @Test
        @DisplayName("Creating a department without a name throws IllegalArgumentException")
        void createDepartmentWithoutName() {
            assertThrows(IllegalArgumentException.class, () -> new Department(""));
        }

        @Test
        @DisplayName("Creating a person with a name")
        void createPerson() {
            assertDoesNotThrow(() -> new Employee("Sindre", "Glomnes", ""));
        }

        @Test
        @DisplayName("Creating a person without last name throws IllegalArgumentException")
        void createPersonWithoutLastName(){
            assertThrows(IllegalArgumentException.class, () -> new Employee("Sindre", "", ""));
        }

        @Test
        @DisplayName("Creating a person without first name throws IllegalArgumentException")
        void createPersonWithoutFirstName(){
            assertThrows(IllegalArgumentException.class, () -> new Employee("", "Glomnes", ""));
        }

    }



}
