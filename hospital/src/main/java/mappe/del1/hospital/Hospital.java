package mappe.del1.hospital;

import java.util.ArrayList;

/**
 * Hospital is a register which contans a list of deparments.
 * Departments can be added to the register. Departments
 * can be retrieved as a list, or individually. A string containing
 * the name of the hospital and every department with all their information
 * can also be retrieved. Hospitals have their own names.
 * @author Sindre Glomnes
 * @version 2021.03.04
 */
public class Hospital {
    private String hospitalName;

    //List of departments in a hospital
    private ArrayList<Department> departments = new ArrayList<>();

    /**
     * Creates an instance of Hospital.
     * @param hospitalName the name of the hospital
     * @throws IllegalArgumentException throws illegal argument exception if
     * hospital does not have a name.
     */
    public Hospital(String hospitalName) {
        if (hospitalName == null || hospitalName.equals("")) {
            throw new IllegalArgumentException("Hospital must have a name.");
        }
        this.hospitalName = hospitalName;
    }

    /**
     * Returns the name of the hospital
     * @return hospital name
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * Return the arraylist of departments
     * @return Arraylist of departments.
     */
    public ArrayList<Department> getDepartments() {
        return departments;
    }

    /**
     * Adds a new department(object department) to the department list.
     * @param department a department object.
     */
    public void addDepartment(Department department) {
        departments.add(department);
    }

    /**
     * Returns a department object if this department's name
     * equals another name. If it does not equal, null is returned.
     * @param departmentName the name of a department
     * @return Object department.
     */
    public Department getDepartment(String departmentName) {
        for (Department d: departments) {
            if (d.getDepartmentName().equals(departmentName)) {
                return d;
            }
        }
        return null;
    }

    /**
     * Returns a string containing the name of the hospital, and all departments
     * in it, with all their employees and patients.
     * @return information about all departments in the hospital.
     */
    @Override
    public String toString() {
        StringBuilder departmentList = new StringBuilder();
        for (Department d: departments) {
            departmentList.append(d.toString());
        }

        return "Hospital: " + hospitalName.toUpperCase() +
                "\n------------------------------------------"
                + departmentList;
    }
}
