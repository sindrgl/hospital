package mappe.del1.hospital;

/**
 *Interface class for setting diagnosis on patient.
 * @author Sindre Glomnes
 * @version 2021.03.04
 */
public interface Diagnosable {
    /**
     * Sets diagnosis to patient.
     * @param diagnosis the patient's diagnosis.
     */
    void setDiagnosis(String diagnosis);
}
