package mappe.del1.hospital;

/**
 *Exception class used to handle removal of non-existing persons.
 * @author Sindre Glomnes
 * @version 2021.03.04
 */
public class RemoveException extends Exception{
    private static final long serialVersionUID = 1L;

    /**
     * Creates an instance of RemoveException.
     * @param errorMessage the error message
     */
    public RemoveException(String errorMessage) {
        super(errorMessage);
    }
}
