package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Employee;
import mappe.del1.hospital.Patient;

/**
 *Doctor extends employee. Can set diagnosis to patients.
 * @author Sindre Glomnes
 * @version 2021.03.04
 */
public abstract class Doctor extends Employee {

    /**
     * Creates an instance of Doctor.
     * @param firstName the first name of doctor
     * @param lastName the last name of doctor
     * @param socialSecurityNumber the social security number of a doctor
     */
    public Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Doctors can set diagnosis on patients.
     * @param patient the patient which the the diagnosis is set to.
     * @param diagnosis the diagnosis given to the patient.
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}
