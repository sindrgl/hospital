package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Patient;

/**
 *General Practitioner extends Doctor. Can set diagnosis to patients.
 * @author Sindre Glomnes
 * @version 2021.03.04
 */
public class GeneralPractitioner extends Doctor {

    /**
     * Creates an instance of GeneralPractitioner.
     * @param firstName the first name of GeneralPractitioner.
     * @param lastName the last name of GeneralPractitioner.
     * @param socialSecurityNumber the social security number of a GeneralPractitioner.
     */
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * GeneralPractitioners can set diagnosis on patients.
     * @param patient the patient which the the diagnosis is set to.
     * @param diagnosis the diagnosis given to the patient.
     */
    public void setDiagnosis(Patient patient, String diagnosis){
        patient.setDiagnosis(diagnosis);
    }
}
