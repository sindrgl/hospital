package mappe.del1.hospital.healthpersonal;

import mappe.del1.hospital.Employee;

/**
 *Nurse extends employee.
 * @author Sindre Glomnes
 * @version 2021.03.04
 */
public class Nurse extends Employee {

    /**
     * Creates an instance of Nurse.
     * @param firstName the first name of Nurse.
     * @param lastName the last name of Nurse.
     * @param socialSecurityNumber the social security number of a Nurse.
     */
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * toString method returns all information about the nurse
     * @return information about the nurse.
     */
    @Override
    public String toString() {
        return super.toString();
    }
}
