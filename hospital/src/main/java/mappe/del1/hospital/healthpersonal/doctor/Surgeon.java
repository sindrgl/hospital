package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Patient;

/**
 *Surgeon extends Doctor. Can set diagnosis to patients.
 * @author Sindre Glomnes
 * @version 2021.03.04
 */
public class Surgeon extends Doctor {

    /**
     * Creates an instance of Surgeon.
     * @param firstName the first name of Surgeon.
     * @param lastName the last name of Surgeon.
     * @param socialSecurityNumber the social security number of a Surgeon.
     */
    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Surgeons can set diagnosis to patients.
     * @param patient the patient which the the diagnosis is set to.
     * @param diagnosis the diagnosis given to the patient.
     */
    public void setDiagnosis(Patient patient, String diagnosis){
        patient.setDiagnosis(diagnosis);

    }
}
