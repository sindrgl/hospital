package mappe.del1.hospital;

/**
 *Abstract class Employee extends Person.
 * @author Sindre Glomnes
 * @version 2021.03.04
 */
public class Employee extends Person {
    /**
     * Creates an instance of Employee.
     * @param firstName the first name of employee.
     * @param lastName the last name of employee.
     * @param socialSecurityNumber the social security number of a employee.
     */
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * toString method returns all information about a employee.
     * @return information about a employee.
     */
    @Override
    public String toString() {
        return super.getClass().getSimpleName() + ": " + super.toString() + "\n";
    }
}
