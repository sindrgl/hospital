package mappe.del1.hospital;

import java.util.Objects;

/**
 * Abstract class Person has the variables used for all types of employees and patients.
 * First name, last name and social security number can be changed and returned. they can also
 * be returned separately, or as one string.
 * @author Sindre Glomnes
 * @version 2021.03.04
 */
public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * Creates an instance of Person.
     * @param firstName the first name of person
     * @param lastName the last name of person
     * @param socialSecurityNumber the social security number of a person
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        if ((firstName == null || firstName.equals("")) || ((lastName == null || lastName.equals("")))) {
            throw new IllegalArgumentException("Person must have a first and last name.");
        }
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     *Returns the first name of a person
     * @return first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Returns the last name of a person
     * @return last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Returns the social security number of a person
     * @return social security number
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * Returns the full name of a person
     * @return full name
     */
    public String getFullName() {
        return firstName + " " + lastName;
    }

    /**
     * Sets the first name of a person
     * @param firstName first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Sets the last name of a person
     * @param lastName last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Sets the social security number of a person
     * @param socialSecurityNumber social security number
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Compares this person to another person to check
     * if this person is the same as the other. It's the same person
     * if they share the same object, or all their parameters equals.
     * @param o the person.
     * @return true if its the same person. False if it's not the same person.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(firstName, person.firstName) &&
                Objects.equals(lastName, person.lastName) &&
                Objects.equals(socialSecurityNumber, person.socialSecurityNumber);
    }


    /**
     * toString methode return a string of that contains the first name, last name and
     * social security number.
     * @return String of information about a person.
     */
    @Override
    public String toString() {
        return firstName + " " + lastName + ", nr: " + socialSecurityNumber;
    }
}
