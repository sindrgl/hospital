package mappe.del1.hospital;

import mappe.del1.hospital.healthpersonal.doctor.Doctor;
import mappe.del1.hospital.healthpersonal.doctor.Surgeon;

/**
 *Client class creates a new hospital, fills it with departments. Tries to remove an existing employee
 * and an non-existing patient. Prints all information.
 * @author Sindre Glomnes
 * @version 2021.03.04
 */
public class HospitalClient {
    public static void main(String[] args) throws RemoveException {
        //Creates a hospital object
        Hospital stOlavs = new Hospital("St. Olavs Hospital");

        //Fills the hospital with patients and employees.
        HospitalTestData.fillRegisterWithTestData(stOlavs);

        //Removes a employee for department Akutten.
        stOlavs.getDepartment("Akutten").remove(new Employee("Odd Even", "Primtallet", ""));

        //Catches the error caused by removing a non-existing patient.
        try {
            stOlavs.getDepartment("Akutten").remove(new Patient("Solan", "Gundersen", ""));

        } catch (RemoveException e) {
            e.printStackTrace();
        }


        //Surgeon sets diagnosis to patient.
        Doctor sindre = new Surgeon("Sindre", "Glomnes", "123");
        stOlavs.getDepartment("Akutten").addEmployee(sindre);
        sindre.setDiagnosis(stOlavs.getDepartment("Akutten").getPatients().get(0), "Feber");

        //Prints all information of the hospital.
        System.out.println(stOlavs.toString());
    }
}
