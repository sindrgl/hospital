package mappe.del1.hospital;

import java.util.ArrayList;
import java.util.Objects;

/**
 *Department is a register which contains lists of all
 * employees and patients registered in it. Both employees and patients
 * can be added to the register, or removed from the register.
 * The lists of patients and employees can both be retrieved separately,
 * or together. Deparments have their own names.
 * @author Sindre Glomnes
 * @version 2021.03.04
 */
public class Department {

    private String departmentName;

    //Lists all employees in this department
    ArrayList<Employee> employees = new ArrayList<>();
    //Lists all patients in this department
    ArrayList<Patient> patients = new ArrayList<>();

    /**
     * Creates an instance of Department.
     * @param departmentName name of department
     * @throws IllegalArgumentException throws illegal argument exception if
     * department does not have a name.
     */
    public Department(String departmentName) {
        if (departmentName == null || departmentName.equals("")) {
            throw new IllegalArgumentException("Department must have a name.");
        }
        this.departmentName = departmentName;
    }

    /**
     * Sets name of a department
     * @param departmentName department name
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Returns the department name
     * @return department name
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Returns list of employees
     * @return employees list of employees.
     */
    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    /**
     * Returns list of employees
     * @return patients list of patients
     */
    public ArrayList<Patient> getPatients() {
        return patients;
    }

    /**
     * Adds a employee to the list of employees.
     * @param employee Object employee
     */
    public void addEmployee(Employee employee) {
        if (!employees.contains(employee)) {
            employees.add(employee);
        }
    }

    /**
     * Adds a patient to the list of patients.
     * @param patient object patient
     */
    public void addPatient(Patient patient) {
        if (!patients.contains(patient)) {
            patients.add(patient);
        }
    }

    /**
     * Removes a person from the department. Throws RemoveException if the person
     * is not found in the lists.
     * @param person object person
     * @throws RemoveException throw remove exception containing a string.
     */
    public void remove(Person person) throws RemoveException{
        if (employees.contains(person) || patients.contains(person)) {
            employees.remove(person);
            patients.remove(person);
        } else {
            throw new RemoveException("Person not found in register.");
        }
    }

    /**
     * Checks if this department equals another.
     * @param o Object of deparment
     * @return true or false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName) &&
                Objects.equals(employees, that.employees) &&
                Objects.equals(patients, that.patients);
    }


    @Override
    public int hashCode() {
        return Objects.hash(departmentName, employees, patients);
    }

    /**
     * Returns a string of all employees and patients in a department.
     * @return Lists all employees and patients as one string
     */
    @Override
    public String toString() {
        StringBuilder employeesList = new StringBuilder("EMPLOYEES:\n");
        StringBuilder patientsList = new StringBuilder("\nPATIENTS:\n");

        for (Employee e: employees) {
            employeesList.append(e.toString());
        }
        for (Patient p: patients) {
            patientsList.append(p.toString());
        }

        return "\nDepartment: " + departmentName.toUpperCase() + "\n--------------------\n"+
                employeesList + patientsList;
    }
}
