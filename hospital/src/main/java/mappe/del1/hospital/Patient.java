package mappe.del1.hospital;

/**
 *Patient extends Person. Patients can be implemented with a diagnosis.
 * @author Sindre Glomnes
 * @version 2021.03.04
 */
public class Patient extends Person implements Diagnosable {
    private String diagnosis = "";

    /**
     * Creates an instance of Patient.
     * @param firstName the first name of patient
     * @param lastName the last name of patient
     * @param socialSecurityNumber the social security number of a patient
     */
    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Returns the first name of a patient
     * @return first name
     */
    public String getDiagnose() {
        return diagnosis;
    }


    /**
     * Sets a diagnosis on the patient.
     * @param diagnosis a string describing the diagnosis
     */
    @Override
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * toString method returns information about a patient.
     * @return information about a patient.
     */
    @Override
    public String toString() {
        return super.toString() +
                ", diagnosis: " + diagnosis +"\n";
    }
}
